#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.colorbar import Colorbar
import sys
import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=FutureWarning)
    import h5py


if __name__ == "__main__":

    # Open file
    h5 = h5py.File(sys.argv[1], "r")

    # Read attributes
    obsid = h5.attrs["observation_id"]
    filename_timestamp = h5.attrs["observation_timestamp"]
    center_frequency = h5.attrs["center_frequency"]
    center_frequency_unit = h5.attrs["center_frequency_unit"]
    script_name = h5.attrs["script_name"]
    baud = h5.attrs["baud"]
    tle0 = h5.attrs["tle_line_0"]
    tle1 = h5.attrs["tle_line_1"]
    tle2 = h5.attrs["tle_line_2"]
    ground_station_id = h5.attrs["ground_station_id"]
    ground_station_lat = h5.attrs["ground_station_lat"]
    ground_station_lon = h5.attrs["ground_station_lon"]
    ground_station_elev = h5.attrs["ground_station_elev"]


    timestamp = h5["waterfall"].attrs["start_time"]
    data_min = h5["waterfall"].attrs["data_min"]
    data_max = h5["waterfall"].attrs["data_max"]
    offset_in_stds = h5["waterfall"].attrs["offset_in_stds"]
    scale_in_stds = h5["waterfall"].attrs["scale_in_stds"]

    offset = h5["waterfall"]["offset"].value
    scale = h5["waterfall"]["scale"].value
    data_8bit = h5["waterfall"]["data"].value
    trel = h5["waterfall"]["relative_time"].value
    tabs = h5["waterfall"]["absolute_time"].value
    freq = h5["waterfall"]["frequency"].value

    # Apply scale and offset
    data = data_8bit.astype("float32") * scale + offset

    # Axis ranges
    tmin, tmax = np.min(tabs), np.max(tabs)
    fmin, fmax = np.min(freq), np.max(freq)

    # Create plot
    fig = plt.figure(figsize=(15, 10))
    gs = gridspec.GridSpec(3, 2, height_ratios=[0.05, 1.0, 0.2], width_ratios=[1.0, 0.1])
    gs.update(left=0.05, right=0.95, bottom=0.08, top=0.93, wspace=0.02, hspace=0.03)

    # Plot waterfall
    ax1 = plt.subplot(gs[1, 0])
    img = ax1.imshow(data.T, origin="lower", aspect="auto", interpolation="None",
                     extent=[tmin, tmax, fmin, fmax],
                     vmin=data_min, vmax=data_max,
                     cmap="viridis")
    ax1.set_ylabel("Frequency (kHz)")
    ax1.set_xticklabels([])
    ax1.grid(alpha=0.1)

    # Plot colorbar
    ax2 = plt.subplot(gs[0, 0])
    cbar = Colorbar(ax=ax2, mappable=img, orientation="horizontal", ticklocation="top")
    cbar.set_label("Power (dB)")

    # Horizontal
    ax3 = plt.subplot(gs[2, 0])
    ax3.plot(tabs, np.mean(data, axis=1))
    ax3.set_xlim(tmin, tmax)
    ax3.set_xlabel("Time (seconds) since %s" % timestamp.decode("utf-8"))
    ax3.set_ylabel("Power (dB)")
    ax3.grid()

    # Vertical
    ax4 = plt.subplot(gs[1, 1])
    ax4.plot(np.mean(data, axis=0), freq)
    ax4.set_ylim(fmin, fmax)
    ax4.set_yticklabels([])
    ax4.set_xlabel("Power (dB)")
    ax4.grid()
    
    plt.show()
